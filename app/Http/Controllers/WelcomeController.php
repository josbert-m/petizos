<?php

namespace App\Http\Controllers;

use App\Models\Pet;

class WelcomeController extends Controller
{
    /**
     * Show the homepage.
     * 
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public function __invoke()
    {
        $pets = Pet::with([
            'paints' => [
                'sizes' => function($query) {
                    $query->orderBy('height', 'desc');
                }
            ]
        ])
        ->get();

        return view('welcome', compact('pets'));
    }
}
