<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrderRequest;
use App\Mail\OrderMailable;
use App\Models\Size;
use Mail;

class OrderController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\OrderRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OrderRequest $request)
    {
        $sizes = explode('x', $request->input('size'));

        $pets = Size::whereRaw(
            "`painting_id` = (SELECT `id` FROM `paintings` WHERE `paintings`.`name` = ? AND `paintings`.`pet_id` = (SELECT `id` FROM `pets` WHERE `pets`.`name` = ? LIMIT 1) LIMIT 1)",
            [$request->input('design'), $request->input('pet')]
        )
        ->whereHeight($sizes[0])
        ->whereWidth($sizes[1])
        ->get();

        $exists = $pets->count();

        if($exists === 0) {
            abort(404);
        }

        Mail::to(config('mail.order_mail'))
        ->send(new OrderMailable(
            $request->input('pet'), 
            $request->input('size'), 
            $request->input('design'), 
            $request->input('name'), 
            $request->input('note'), 
            $request->input('contact'), 
            $request->file('picture')
        ));

        return response()
        ->json([
            'message' => 'Pedido registrado.'
        ], 201);
    }
}
