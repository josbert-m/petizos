<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Image;
use Storage;

class ImageController extends Controller
{
    public function __invoke(Request $request, $path)
    {
        $path = str_replace('++', '/', $path);

        if(!Storage::disk('local')->exists($path)) {
            return abort(404);    
        }

        if($request->query('min', false) === 'on') {
            $minimized = "cache/minimized_{$path}";

            if(Storage::disk('local')->exists($minimized)) {
                return Storage::response($minimized);
            }

            $fullpath = Storage::path($path);
            $image = Image::make($fullpath);

            $width = $image->width();
            $height = $image->height();

            $crop = min($width, $height);
            $x = max($width, $height) === $width ? ceil(($width - $height) / 2) : 0;
            $y = max($width, $height) === $width ? 0 : ceil(($height - $width) / 2);

            $image = $image->crop($crop, $crop, $x, $y)
            ->widen(128);

            Storage::put($minimized, $image->stream());

            return $image->response();
        }

        return Storage::response($path);
    }
}
