<?php

namespace App\Http\Requests;

use App\Rules\NotNullable;
use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'pet' => [
                'exists:pets,name', 'required'
            ],
            'size' => [
                'required', 'regex:/^[0-9]{2,}x[0-9]{2,}$/'
            ],
            'design' => [
                'required', new NotNullable
            ],
            'contact' => [
                'required', new NotNullable
            ],
            'name' => [
                'nullable', 'string'
            ],
            'picture' => [
                'required', 'file', 'max:8192', 'mimes:png,jpg,jpeg'
            ],
            'note' => [
                'nullable', 'min:3'
            ]
        ];
    }
}
