<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Http\Testing\MimeType;
use Illuminate\Http\UploadedFile;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Str;

class OrderMailable extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(
        public string $pet, 
        public string $size, 
        public string $design, 
        public string|null $name = null, 
        public string|null $note = null, 
        public string $contact,
        public UploadedFile $picture
    )
    { }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $slug = Str::slug($this->contact);

        return $this->subject('Nuevo Pedido de ' . Str::title(mb_strtolower($this->pet, 'UTF-8')))
        ->attachData(file_get_contents($this->picture->path()), "{$slug}.{$this->picture->extension()}", [
            'mime' => MimeType::get($this->picture->extension())
        ])
        ->view('mails.order', [
            'pet' => $this->pet,
            'contact' => $this->contact,
            'design' => $this->design,
            'size' => $this->size,
            'name' => $this->name,
            'note' => $this->note
        ]);
    }
}
