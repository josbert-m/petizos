<?php

namespace App\View\Components;

use Illuminate\View\Component;

/**
 * @property string $bgColor
 * @property string $textColor
 * @property string $tag
 * @property string|null
 * @property string|null $xText
 */
class Button extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(
        public string $bgColor = 'bg-transparent',
        public string $textColor = 'text-black',
        public string $tag = 'button',
        public string $role = 'button',
        public string|null $link = null,
        public string|null $xText = null,
        public string|null $xKey = null,
        public string|null $xClass = null,
        public string|null $xClick = null,
        public bool $isInput = false,
        public bool $hashLink = false
    )
    {

    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.button');
    }
}
