<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        /**
         * Share global views data
         */
        View::share('navbar_links', [
            'Inicio' => '/#inicio',
            'Nosotros' => '/#nosotros',
            'Pasos' => '/#pasos',
            'Personalizar' => '/#personalizar'
        ]);

        View::share('navbar_social', [
            'fa-whatsapp' => 'wa.me/message/63S6ICFYUMSCO1',
            'fa-instagram' => 'https://www.instagram.com/petizos.pe',
            'fa-facebook' => 'https://www.facebook.com/Petizos-Per%C3%BA-113150484490163'
        ]);

        View::share('footer_social', [
            'fa-facebook' => 'https://www.facebook.com/Petizos-Per%C3%BA-113150484490163',
            'fa-instagram' => 'https://www.instagram.com/petizos.pe',
            'fa-tiktok' => 'https://www.tiktok.com/@petizos.pe'
        ]);
    }
}
