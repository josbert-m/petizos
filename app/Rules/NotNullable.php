<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\InvokableRule;

class NotNullable implements InvokableRule
{
    /**
     * Run the validation rule.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     * @return void
     */
    public function __invoke($attribute, $value, $fail)
    {
        if(is_null($value)) {
            $fail('El campo :attribute es obligatorio.');
        }

        if(is_string($value) && strlen($value) === 0) {
            $fail('El campo :attribute es obligatorio.');
        }
    }
}
