<?php

namespace App\Models;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Painting
 *
 * @property int $id
 * @property string $name
 * @property int $pet_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \App\Models\Pet $pet
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Size[] $sizes
 * @property-read int|null $sizes_count
 * @method static \Illuminate\Database\Eloquent\Builder|Painting newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Painting newQuery()
 * @method static \Illuminate\Database\Query\Builder|Painting onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Painting query()
 * @method static \Illuminate\Database\Eloquent\Builder|Painting whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Painting whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Painting whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Painting whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Painting wherePetId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Painting whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Painting withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Painting withoutTrashed()
 * @mixin \Eloquent
 */
class Painting extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'Y-m-d H:i:s.u';

    /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d\TH:i:s.uO');
    }

    /**
     * Inverse One to Many relationship with Pet model
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function pet()
    {
        return $this->belongsTo(Pet::class);
    }

    /**
     * One to Many relationship with Size model
     * 
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sizes()
    {
        return $this->hasMany(Size::class);
    }
}
