<?php

namespace App\Models;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Size
 *
 * @property int $id
 * @property string $path
 * @property int $price
 * @property int $painting_id
 * @property int $width
 * @property int $height
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \App\Models\Painting|null $paint
 * @method static \Illuminate\Database\Eloquent\Builder|Size newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Size newQuery()
 * @method static \Illuminate\Database\Query\Builder|Size onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Size query()
 * @method static \Illuminate\Database\Eloquent\Builder|Size whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Size whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Size whereHeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Size whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Size wherePaintingId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Size wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Size wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Size whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Size whereWidth($value)
 * @method static \Illuminate\Database\Query\Builder|Size withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Size withoutTrashed()
 * @mixin \Eloquent
 */
class Size extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'Y-m-d H:i:s.u';

    /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d\TH:i:s.uO');
    }

    /**
     * Inverse One to Many relationship with Painting model
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function paint()
    {
        return $this->belongsTo(Painting::class);
    }
}
