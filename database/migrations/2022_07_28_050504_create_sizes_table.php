<?php

use App\Models\Painting;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sizes', function (Blueprint $table) {
            $table->id();
            $table->string('path')->unique();
            $table->bigInteger('price', unsigned: true);
            $table->foreignIdFor(Painting::class)->references('id')->on('paintings')->onDelete('cascade');
            $table->integer('width', unsigned: true)->default(40);
            $table->integer('height', unsigned: true)->default(70);
            $table->timestamps(3);
            $table->softDeletes(precision: 3);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sizes');
    }
};
