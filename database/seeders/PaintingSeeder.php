<?php

namespace Database\Seeders;

use App\Models\Painting;
use App\Models\Pet;
use App\Models\Size;
use Illuminate\Database\Seeder;
use Storage;

class PaintingSeeder extends Seeder
{
    /**
     * Type pets
     * 
     * @var array $pet_types
     */
    protected $pet_types = [
        'dogs' => 'Perro',
        'cats' => 'Gato'
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pets = ['dogs', 'cats'];

        foreach($pets as $pet) {
            $raw = file_get_contents(resource_path("img/{$pet}/{$pet}.json"));
            $data = collect(json_decode($raw, true))->groupBy('name');
            $type = mb_strtoupper($this->pet_types[$pet]);
            
            $type = Pet::whereName($type)
            ->select('id')
            ->first();

            $data->each(function ($value, $key) use($type, $pet) {
                $paint = new Painting([
                    'name' => mb_strtoupper($key)
                ]);

                $paint->pet()->associate($type);
                $paint->save();

                $value->each(function($file) use($pet, $paint) {
                    $path = "img/{$pet}/{$file['original']}";
                    $new_path = "{$pet}/{$file['new']}";

                    preg_match("/([0-9]{2})x([0-9]{2})(?=\.(jpeg|jpg|png)$)/i", $file['new'], $matches);

                    $size = explode('x', $matches[0]);

                    Storage::put($new_path, file_get_contents(resource_path($path)));

                    $size_model = new Size([
                        'path' => $new_path,
                        'price' => $file['price'],
                        'width' => $size[1],
                        'height' => $size[0]
                    ]);

                    $paint->sizes()->save($size_model);
                    usleep(1000);
                });
            });
        }
    }
}
