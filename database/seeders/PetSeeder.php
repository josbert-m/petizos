<?php

namespace Database\Seeders;

use App\Models\Pet;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pets = ['Perro', 'Gato'];

        foreach($pets as $pet) {
            $model = new Pet([
                'name' => mb_strtoupper($pet)
            ]);

            $model->save();
            usleep(1000);
        }
    }
}
