/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./resources/**/*.blade.php",
    "./resources/**/*.js",
    "./resources/**/*.vue",
    "./app/View/Components/**/*.php"
  ],
  theme: {
    extend: {
      animation: {
        'ping-fast': 'ping 750ms ease-out infinite',
      }
    },
    fontFamily: {
      sans: ["'Lato'", "sans-serif"]
    }
  },
  plugins: [
    require('tailwind-bootstrap-grid')({
      containerMaxWidths: {
        xl: '1140px'
      }
    })
  ],
}
