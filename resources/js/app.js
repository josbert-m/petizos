import Alpine from "alpinejs"
import axios from "axios"

document.addEventListener('DOMContentLoaded', () => {

    const petArtContainer = document.querySelector('#container-pet-arts')
    const petArt = document.querySelector('#pet-arts') 

    let petArtHeight = petArt.getBoundingClientRect().height

    petArtContainer.style.height = `${petArtHeight}px`

    /**
     * Hashtag links scroll
     */
    const hashLinks = document.querySelectorAll('[data-hash-link="true"]')

    const toScroll = event => {
        event.preventDefault()
        const toElement = document.querySelector(event.target.hash)

        let offset = toElement.getBoundingClientRect().top 

        window.scroll({
            top: offset,
            behavior: 'smooth'
        })
    }

    hashLinks.forEach(el => {
        el.addEventListener('click', toScroll)
    })

}, false)

document.addEventListener('alpine:init', () => {

    Alpine.store('homeStore', {
        ajax_loading: false,
        bg_content_loader: 'linear-gradient(90deg, rgb(251 113 133) 50%, rgb(0 0 0) 50%)',
        rotate_content_loader: 'rotate(0deg)',
        bg_left: '#000',
        bg_right: 'transparent',
        current_percentage: 0,
        alerts: [],
        /**
         * Methods
         */
        setState(value) {
            this.ajax_loading = value
        },
        setPercentage(value) {
            if(value > 0) {
                let to_update = value - this.current_percentage

                let ms =  500 / to_update

                for (let i = 0; i < to_update; i++) {
                    setTimeout(() => {
                        let current = (this.current_percentage + 1) >= 100 ? 100 : (this.current_percentage + 1)

                        if(current === 51) {
                            this.bg_right = '#fb7185',
                            this.bg_left = 'transparent'
                        }

                        this.current_percentage = current
                        this.rotate_content_loader = `rotate(${(360 / 100) * current}deg)`
                    }, Math.round(ms))
                }
            }
            else {
                this.bg_right = 'transparent',
                this.bg_left = '#000'
                this.current_percentage = 0
                this.rotate_content_loader = 'rotate(0deg)'
            }
        }
    })

    Alpine.data('petArts', () => ({
        showDog: true, 
        showCat: false,
        init() {
            setInterval(() => {
                if(this.showDog) {
                    this.showDog = false

                    setTimeout(() => {
                        this.showCat = true
                    }, 700)
                }

                if(this.showCat) {
                    this.showCat = false

                    setTimeout(() => {
                        this.showDog = true
                    }, 700)
                }
            }, 4000)
        }
    }))

    Alpine.data('orderSection', () => ({
        pets: [],
        pet_type: null,
        paints: [],
        sizes: [],
        selected_size: null,
        selected_design: null,
        pet_name: null,
        pet_photo: null,
        real_pet_photo: null,
        contact: null,
        design_notes: null,
        file_input_text: 'click aquí',
        design_name: null,
        validations: {
            'pet': null,
            'size': null,
            'design': null,
            'name': null,
            'picture': null,
            'contact': null,
            'note': null
        },
        /**
         * Methods
         */
        selectDesign(el) {
            let resultSize = el.sizes.filter(item => {
                return `${item.height}x${item.width}` === this.selected_size
            })

            if(resultSize.length === 0) {
                this.selected_design = el.sizes[0]
            }

            this.selected_design = resultSize[0]

            this.sizes = el.sizes.map(size => {
                return `${size.height}x${size.width}`
            })

            this.design_name = el.name
            this.validations.design = null
        },
        toCurrency(num) {
            let text = num.toString()
            let matches = text.match(/[0-9]{2}$/)
            let match = matches[0]
            let float = text.replace(/[0-9]{2}$/, `.${match}`)
            float = parseFloat(float)

            return float.toLocaleString('es-ES', {
                minimumFractionDigits: 2, 
                maximumFractionDigits: 2
            })
        },
        send() {
            let inputs = {
                'pet': this.pet_type,
                'size': this.selected_size,
                'design': this.design_name,
                'name': this.pet_name,
                'picture': this.real_pet_photo,
                'contact': this.contact,
                'note': this.design_notes
            }

            let form = new FormData()

            Object.keys(inputs).forEach(key => {
                if(inputs[key] !== null && inputs[key] !== undefined) {
                    form.append(key, inputs[key])
                }
            })

            this.$store.homeStore.setState(true)

            axios({
                method: 'post',
                url: 'api/order',
                data: form,
                headers: {
                    'Content-Type': 'multipart/form-data',
                    'Accept': 'application/json'
                },
                onUploadProgress: progressEvent => {
                    if(progressEvent.lengthComputable) {
                        this.$store.homeStore.setPercentage(Math.round((progressEvent.loaded * 100) / progressEvent.total))
                    }
                    else {
                        this.$store.homeStore.setPercentage(99)
                    }
                }
            })
            .then(res => {
                this.$store.homeStore.setState(false)
                this.$store.homeStore.setPercentage(0)

                this.$store.homeStore.alerts.push({
                    type: 'success',
                    message: '¡Pedido enviado con éxito!'
                })
            })
            .catch(error => {
                this.$store.homeStore.setState(false)
                this.$store.homeStore.setPercentage(0)

                if(error.response.status === 422) {
                    let inputErrors = Object.keys(error.response.data.errors)

                    inputErrors.forEach(name => {
                        this.validations[name] = error.response.data.errors[name][0]
                    })

                    this.$store.homeStore.alerts.push({
                        type: 'danger',
                        message: 'Por favor, corrija los errores encontrados'
                    })
                }

                if(error.response.status === 404) {
                    this.$store.homeStore.alerts.push({
                        type: 'danger',
                        message: 'Diseño no encontrado'
                    })
                }

                if(error.response.status === 500) {
                    this.$store.homeStore.alerts.push({
                        type: 'danger',
                        message: 'No se pudo procesar su orden, por favor intente más tarde'
                    })
                }
            })
        },
        /**
         * Created hook
         */
        init() {
            const customize = document.querySelector('#personalizar')

            this.pets = JSON.parse(customize.dataset.orderJson)
            customize.dataset.orderJson = ''

            this.pet_type = this.pets[0].name
            this.paints = this.pets[0].paints
            this.sizes = this.pets[0].paints[0].sizes.map(size => {
                return `${size.height}x${size.width}`
            })
            this.selected_design = this.pets[0].paints[0].sizes[0]
            this.design_name = this.pets[0].paints[0].name
            this.selected_size = `${this.selected_design.height}x${this.selected_design.width}`
            
            /**
             * Watcher for pet_type
             */
            this.$watch('pet_type', val => {
                this.validations.pet = null

                let result = this.pets.filter(item => {
                    return item.name === val
                })

                this.paints = result[0].paints
                this.sizes = result[0].paints[0].sizes.map(size => {
                    return `${size.height}x${size.width}`
                })
                this.selected_design = result[0].paints[0].sizes[0]
                this.design_name = result[0].paints[0].name
                this.selected_size = `${this.selected_design.height}x${this.selected_design.width}`
            })

            /**
             * Watcher for pet_photo
             */
            this.$watch('pet_photo', () => {
                this.validations.picture = null

                this.real_pet_photo = this.$refs.input_pet_photo.files[0]
                this.file_input_text = this.$refs.input_pet_photo.files[0].name
            })

            /**
             * Watcher for selected_size
             */
            this.$watch('selected_size', val => {
                this.validations.size = null

                let paint = this.paints.filter(item => {
                    return this.selected_design.painting_id === item.id
                })

                let design = paint[0].sizes.filter(size => {
                    return `${size.height}x${size.width}` === val
                })

                if(design.length > 0) {
                    this.selected_design = design[0]
                }
            })
        }
    }))

    document.querySelector('body').classList.remove('overflow-hidden')
    document.querySelector('#loading-page').remove()

})

Alpine.start()