@extends('layouts.base')

@section('titlePage', 'Retrato creativo de tu mascota')

@section('content')
    <section class="pt-6 bg-teal-400" id="inicio">
        <div class="container">
            <div class="row row-cols-1 md:row-cols-2">

                <div class="col px-4">
                    <div class="max-w-lg mx-auto mb-12">
                        <h1 class="text-5xl md:text-7xl mb-5 md:mb-7 font-black font-sans text-slate-900">
                            Personaliza el retrato creativo de tu mascota.
                        </h1>
                        <x-button bg-color="ml-2 bg-rose-500 hover:bg-rose-600 focus:bg-rose-600 focus:shadow-[0_0_1px_4px_rgba(244,63,94,0.75)]" 
                        text-color="text-white" 
                        tag="a" 
                        link="{{ route('home') . '/#personalizar' }}"
                        hash-link
                        >
                            Empecemos
                        </x-button>
                    </div>
                </div>

                <div class="col"
                x-data="petArts">
                    <figure class="max-w-xl overflow-hidden relative" id="container-pet-arts">
                        <img x-show="showDog"
                        x-transition:enter-start="opacity-0 translate-x-32"
                        x-transition:enter-end="opacity-100 translate-x-0"
                        x-transition:leave-start="opacity-100 translate-x-0"
                        x-transition:leave-end="opacity-0 translate-x-32"
                        src="{{ asset('img/dog_art.png') }}" class="w-full mx-auto transition-all ease-out duration-700 absolute" alt="Ilustracion de un perro" id="pet-arts"
                        >

                        <img x-show="showCat"
                        x-transition:enter-start="opacity-0 translate-x-32"
                        x-transition:enter-end="opacity-100 translate-x-0"
                        x-transition:leave-start="opacity-100 translate-x-0"
                        x-transition:leave-end="opacity-0 translate-x-32" 
                        src="{{ asset('img/cat_art.png') }}" class="w-full mx-auto transition-all ease-out duration-700 absolute" alt="Ilustracion de un Gato"
                        >
                    </figure>
                </div>

            </div>
        </div>
    </section>

    <section class="pt-12 pb-6" id="nosotros">
        <div class="container">
            <div class="row row-cols-1 md:row-cols-3">
                <div class="col md:col-6 md:order-2 flex flex-col justify-center">
                    <h2 class="text-5xl font-black font-sans text-center text-rose-500 mb-10">
                        ¿Quienes somos?
                    </h2>
                    <p class="text-center max-w-xs mx-auto text-xl leading-8 text-gray-800 mb-8">
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet.
                    </p>
                </div>

                <div class="col md:col-3 md:order-1">
                    <figure class="max-w-sm mx-auto">
                        <img src="{{ asset('img/kratos.jpg') }}" alt="Kratos" class="w-full drop-shadow-[2px_2px_8px_rgba(0,0,0,.75)] filter">
                    </figure>
                </div>

                <div class="col md:col-3 hidden md:flex md:order-3 px-0 items-end">
                    <figure class="max-w-sm mx-auto">
                        <img src="{{ asset('img/men-with-dog.png') }}" alt="Kratos" class="w-full -mb-6">
                    </figure>
                </div>
            </div>
        </div>
    </section>

    <section id="pasos" class="bg-purple-400 pt-14 pb-4">
        <div class="container">
            <h2 class="text-amber-300 text-6xl text-center font-black mb-12">
                ¡Es fácil!
            </h2>

            <div class="row row-cols-1 md:row-cols-3" role="list">
                <div class="col group" role="listitem">
                    <h3 class="text-white text-4xl text-center font-sans font-bold max-w-xs mx-auto mb-8">
                        <span class="text-amber-300">1.</span> Elige el tipo de tu mascota.
                    </h3>

                    <div class="max-w-xs mx-auto py-8">
                        <div class="row row-cols-2">
                            <div class="col group-hover:animate-bounce -translate-y-1/4">
                                <div class="-rotate-12">
                                    {!! file_get_contents(resource_path('img/svg/dog.svg')) !!}
                                </div>
                            </div>
                            <div class="col group-hover:animate-bounce -translate-y-1/4">
                                <div class="rotate-12 p-2">
                                    {!! file_get_contents(resource_path('img/svg/cat.svg')) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col group" role="listitem">
                    <h3 class="text-white text-4xl text-center font-sans font-bold max-w-xs mx-auto mb-8">
                        <span class="text-amber-300">2.</span> Sube su foto.
                    </h3>

                    <div class="max-w-xs mx-auto">
                        <div class="row row-cols-1 group-hover:animate-[bounce-easy_1s_infinite] -translate-y-[12.5%]">
                            <div class="col px-8">
                                <figure class="w-full pt-16">
                                    <img src="{{ asset('img/gallery-pets.png') }}" alt="Galeria de mascotas">
                                </figure>
                            </div>

                            <div class="col">
                                <span class="block text-center text-amber-300 text-5xl">
                                    <i class="fa-solid fa-arrow-up-from-bracket"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col group" role="listitem">
                    <h3 class="text-white text-4xl text-center font-sans font-bold max-w-xs mx-auto mb-10">
                        <span class="text-amber-300">3.</span> Te entregamos tu previsualización gratuita en menos 24 horas.
                    </h3>

                    <div class="max-w-xs mx-auto">
                        <div class="row row-cols-1 group-hover:animate-bounce -translate-y-1/4">
                            <div class="col pt-10">
                                <span class="block text-center text-amber-300 text-8xl">
                                    <i class="fa-solid fa-paper-plane"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="personalizar" class="py-12" data-order-json="{{ json_encode($pets) }}"
    x-data="orderSection"
    >
        <h2 class="text-rose-500 text-6xl text-center font-black mb-12">
            Empecemos a personalizar
        </h2>

        <div class="container">
            <div class="row row-cols-1 md:row-cols-2">

                <div class="col md:col-5 md:pt-16">
                    <figure class="max-w-sm md:max-w-[256px] md:mt-8 mx-auto mb-6 shadow-[-6px_6px_12px_-3px_rgba(0,0,0,0.5)]">
                        <img :src="'https://petizosperu.com/file-stream/' + selected_design.path.replace(/\//g, '++')" 
                        alt="Cuadro de tu mascota"
                        class="min-w-full"
                        >
                    </figure>

                    <h3 class="text-black text-center font-sans font-black text-4xl mb-3 capitalize"
                    x-text="design_name.toLowerCase()"
                    >             
                    </h3>

                    <h3 class="text-black text-center font-sans font-black text-2xl mb-10"
                    x-text="'S/ ' + toCurrency(selected_design.price)"
                    >
                    </h3>
                </div>

                <div class="col md:col-7">
                    <form id="order-form" enctype="multipart/form-data">

                        <div class="mb-4 max-w-xl mx-auto">
                            <label class="block text-xl font-sans text-black mb-4 px-8 font-bold">
                                1. ¿Qué tipo de mascota tienes? *
                            </label>

                            <div class="row row-cols-2">

                                <template x-for="(pet, index) in pets">
                                    <div class="col pb-8"
                                    :class="{ 'pl-9': index % 2 === 0, 'pr-9': index % 2 !== 0 }">
                                        
                                        <x-button role="radio"
                                        bg-color="w-full border-4 border-black hover:bg-black focus:shadow-[0_0_1px_4px_rgba(0,0,0,0.75)]" 
                                        text-color="text-white hover:text-white capitalize" 
                                        x-text="pet.name.toLowerCase()"
                                        x-key="'button-pet-' + pet.id"
                                        x-class="{ 'bg-black': pet_type === pet.name, 'text-black': pet_type !== pet.name }"
                                        x-click="pet_type = pet.name"
                                        >
                                        </x-button>
                    
                                    </div>
                                </template>

                            </div>

                            <p class="text-red-500 font-sans text-base px-8 pb-6 font-bold capitalize"
                            x-show="validations.pet !== null && validations.pet !== undefined"
                            x-text="validations.pet"
                            >
                            </p>
                        </div>

                        <div class="mb-4 max-w-xl mx-auto">
                            <label class="block text-xl font-sans text-black mb-4 px-8 font-bold">
                                2. ¿Qué tamaño deseas? *
                            </label>

                            <div class="row row-cols-2">

                                <template x-for="(size, index) in sizes">
                                    <div class="col pb-8"
                                    :class="{ 'pl-9': index % 2 === 0, 'pr-9': index % 2 !== 0 }">
                                        
                                        <x-button role="radio"
                                        bg-color="w-full border-4 border-black hover:bg-black focus:shadow-[0_0_1px_4px_rgba(0,0,0,0.75)]" 
                                        text-color="text-white hover:text-white" 
                                        x-text="size + ' cm'"
                                        x-key="'button-size-' + size"
                                        x-class="{ 'bg-black': size === selected_size, 'text-black': size !== selected_size }"
                                        x-click="selected_size = size"
                                        >
                                        </x-button>
                    
                                    </div>
                                </template>

                            </div>

                            <p class="text-red-500 font-sans text-base px-8 pb-6 font-bold capitalize"
                            x-show="validations.size !== null && validations.size !== undefined"
                            x-text="validations.size"
                            >
                            </p>
                        </div>

                        <div class="mb-4 max-w-xl mx-auto">
                            <label class="block text-xl font-sans text-black mb-4 px-8 font-bold">
                                3. ¿Qué diseño buscas? *
                            </label>

                            <div class="row row-cols-4">
                                <template x-for="paint in paints">
                                    <div class="col p-3 flex flex-col items-center">
                                        <figure class="cursor-pointer mb-2 group focus:outline-0 max-w-full" role="radio" tabindex="0"
                                        @keydown.enter="selectDesign(paint)"
                                        @click="selectDesign(paint)"
                                        >
                                            <img :src="'https://petizosperu.com/file-stream/' + paint.sizes[0].path.replace(/\//g, '++') + '?min=on'" 
                                            :alt="paint.name"
                                            class="rounded-full min-w-full mx-auto border-4 transition-all duration-300 hover:scale-110 group-focus:shadow-[0_0_1px_4px_rgba(0,0,0,0.75)]"
                                            :class="{ 'border-black': selected_design.painting_id === paint.id, 'border-transparent': selected_design.painting_id !== paint.id, 'shadow-md': selected_design.painting_id === paint.id }"
                                            >
                                        </figure>
                                        <span class="text-center capitalize font-bold font-sans py-1 px-2 rounded-full transition-colors duration-300 leading-4" 
                                        x-text="paint.name.toLowerCase()"
                                        :class="{ 'bg-black': selected_design.painting_id === paint.id, 'text-white': selected_design.painting_id === paint.id, 'text-black': selected_design.painting_id !== paint.id }"
                                        >
                                        </span>
                                    </div>
                                </template>
                            </div>

                            <p class="text-red-500 font-sans text-base px-8 pb-6 font-bold capitalize"
                            x-show="validations.design !== null && validations.design !== undefined"
                            x-text="validations.design"
                            >
                            </p>
                        </div>

                        <div class="mb-4 max-w-xl mx-auto">
                            <label class="block text-xl font-sans text-black mb-4 px-8 font-bold">
                                4. Indica el nombre de tu mascota
                            </label>

                            <div class="max-w-md mx-auto">
                                <p class="mb-4 text-gray-500 font-sans text-xl leading-6">
                                    Puedes dejar en blanco si no quieres que aparezca el nombre de tu mascota.
                                </p>

                                <input type="text" 
                                x-model="pet_name"
                                class="bg-gray-200 rounded-full px-7 py-4 mb-6 font-sans text-gray-900 text-lg font-bold w-full block focus:shadow-[0_0_1px_4px_rgba(0,0,0,0.5)] focus:outline-0"
                                :class="{ 'shadow-[0_0_1px_4px_rgba(239,68,68,0.5)]': validations.name !== null && validations.name !== undefined }"
                                @input="validations.name = null"
                                >
                            </div>

                            <p class="text-red-500 font-sans text-base px-8 pb-6 font-bold capitalize"
                            x-show="validations.name !== null && validations.name !== undefined"
                            x-text="validations.name"
                            >
                            </p>
                        </div>

                        <div class="mb-4 max-w-xl mx-auto">
                            <label class="block text-xl font-sans text-black mb-4 px-8 font-bold">
                                5. Adjunta la foto *
                            </label>

                            <div class="max-w-md mx-auto">
                                <p class="mb-4 text-gray-500 font-sans text-xl leading-6">
                                    Tener en cuenta que la fotograafía tenga buena iluminación y que no salga cortada la mascota en la foto.
                                </p>

                                <div class="bg-gray-200 rounded-full text-center cursor-pointer px-7 py-4 mb-7 font-sans text-gray-900 text-lg font-bold w-full block focus:shadow-[0_0_1px_4px_rgba(0,0,0,0.5)] focus:outline-0"
                                :class="{ 'shadow-[0_0_1px_4px_rgba(239,68,68,0.5)]': validations.picture !== null && validations.picture !== undefined }"
                                role="input"
                                tabindex="0"
                                @click="$refs.input_pet_photo.click()"
                                @keydown.enter="$refs.input_pet_photo.click()"
                                >
                                    <i class="fa-solid fa-arrow-up-from-bracket mr-1"></i> <span x-text="file_input_text" class="max-w-[140px] truncate inline-block align-bottom"></span>

                                    <input type="file" class="hidden"
                                    x-model="pet_photo"
                                    x-ref="input_pet_photo"
                                    >
                                </div>
                            </div>

                            <p class="text-red-500 font-sans text-base px-8 pb-6 font-bold capitalize"
                            x-show="validations.picture !== null && validations.picture !== undefined"
                            x-text="validations.picture"
                            >
                            </p>
                        </div>

                        <div class="mb-4 max-w-xl mx-auto">
                            <label class="block text-xl font-sans text-black mb-4 px-8 font-bold">
                                6. Número de celular o correo electrónico *
                            </label>

                            <div class="max-w-md mx-auto">
                                <p class="mb-4 text-gray-500 font-sans text-xl leading-6">
                                    Escribe el método de envío de la previsualización que deseas recibir.
                                </p>

                                <input type="text" 
                                x-model="contact"
                                class="bg-gray-200 rounded-full px-7 py-4 mb-6 font-sans text-gray-900 text-lg font-bold w-full block focus:shadow-[0_0_1px_4px_rgba(0,0,0,0.5)] focus:outline-0"
                                :class="{ 'shadow-[0_0_1px_4px_rgba(239,68,68,0.5)]': validations.contact !== null && validations.contact !== undefined }"
                                @input="validations.contact = null"
                                >
                            </div>

                            <p class="text-red-500 font-sans text-base px-8 pb-6 font-bold capitalize"
                            x-show="validations.contact !== null && validations.contact !== undefined"
                            x-text="validations.contact"
                            >
                            </p>
                        </div>

                        <div class="mb-4 max-w-xl mx-auto">
                            <label class="block text-xl font-sans text-black mb-4 px-8 font-bold">
                                8. Notas al diseñador
                            </label>

                            <div class="max-w-md mx-auto">
                                <p class="mb-4 text-gray-500 font-sans text-xl leading-6">
                                    Puedes dejar algún comentario adicional sobre algunos detalles que quisieras aclarar.
                                </p>

                                <textarea rows="4"
                                x-model="design_notes"
                                class="bg-gray-200 rounded-3xl resize-y px-7 py-4 mb-6 font-sans text-gray-900 text-lg font-bold w-full block focus:shadow-[0_0_1px_4px_rgba(0,0,0,0.5)] focus:outline-0"
                                :class="{ 'shadow-[0_0_1px_4px_rgba(239,68,68,0.5)]': validations.note !== null && validations.note !== undefined }"
                                @input="validations.note = null"
                                >
                                </textarea>
                            </div>

                            <p class="text-red-500 font-sans text-base px-8 pb-6 font-bold capitalize"
                            x-show="validations.note !== null && validations.note !== undefined"
                            x-text="validations.note"
                            >
                            </p>
                        </div>

                        <div class="max-w-md mx-auto">
                            <ul class="mb-6 text-gray-500 font-sans text-xl leading-6">
                                <li class="mb-2"><span class="mr-2 text-gray-800">-</span> Retratos impresos sobre CELTEX (3mm).</li>
                                <li><span class="mr-2 text-gray-800">-</span> Envío gratuito a cualquier parte de Lima.</li>
                            </ul>

                            <x-button is-input
                            bg-color="bg-rose-500 hover:bg-rose-600 w-full max-w-[280px] focus:bg-rose-600 focus:shadow-[0_0_1px_4px_rgba(244,63,94,0.75)]"
                            text-color="text-white"
                            x-click="send()"
                            >
                                Enviar
                            </x-button>
                        </div>

                    </form>
                </div>

            </div>
        </div>
    </section>
@endsection