@extends('layouts.base')

@section('titlePage', 'No encontrado')

@section('content')
    <section class="py-8">
        <div class="container">
            <div class="row py-8">
                <div class="col">
                    <h1 class="font-sans font-black text-rose-500 text-7xl text-center mb-2">
                        404
                    </h1>
                    <h2 class="font-sans font-black text-rose-500 text-4xl text-center capitalize mb-2">
                        no encontrado
                    </h2>
                    <p class="text-center">
                        <a href="{{ route('home') }}" class="capitalize text-slate-700 text-xl font-bold font-sans hover:text-slate-900 transition-colors duration-300">Ir al inicio.</a>
                    </p>
                </div>
            </div>
        </div>
    </section>
@endsection