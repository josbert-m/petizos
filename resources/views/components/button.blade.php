<{{ $isInput ? 'input' : $tag }} class="{{ $bgColor }} {{ $textColor }} px-6 py-3 rounded-full text-xl font-bold inline-block focus:outline-0 transition-all duration-200" 
role="{{ $role }}" 
{{ !is_null($link) ? 'href=' . $link : '' }}
{!! is_null($xText) ? '' : 'x-text="' . $xText . '"' !!}
{!! is_null($xKey) ? '' : ':key="' . $xKey . '"' !!}
{!! is_null($xClass) ? '' : ':class="' . $xClass . '"' !!}
{!! is_null($xClick) ? '' : '@click.prevent="' . $xClick . '"' !!}
@if($hashLink)
  data-hash-link="true"
@endif

@if($isInput)
  type="submit"
  value="{!! $slot !!}"
@endif
>
@if(!$isInput)
    {{ $slot }}
  </{{ $tag }}>
@endif