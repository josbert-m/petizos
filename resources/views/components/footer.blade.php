<footer class="bg-rose-500 pt-12 pb-8">
  <div class="container">
    <div class="row row-cols-1 md:row-cols-3">

      <div class="col mb-6 md:mb-0">
        <div class="max-w-md mx-auto">
          <h2 class="text-4xl font-sans text-white font-black mb-4">
            Hey!
          </h2>

          <p class="text-white font-sans text-xl mb-4 max-w-xs">
            No olvides seguirnos en nuestras redes:
          </p>

          <ul class="list-none flex flex-row h-full items-center">
            @foreach ($social as $icon => $link)
              <li class="flex" role="menuitem">
                <a href="{{ $link }}" class="text-white text-3xl mr-3 hover:text-slate-900 transition-colors duration-200">
                  <i class="fa-brands {{ $icon }}"></i>
                </a>
              </li>
            @endforeach
          </ul>
        </div>

      </div>

      <div class="col mb-6 md:mb-0">
        <div class="max-w-md mx-auto">
          <p class="text-white font-sans text-xl mb-4 font-bold">
            Contáctanos:
          </p>

          <ul class="">
            <li class="text-white text-xl mb-3">
              <i class="fa-solid fa-phone mr-3"></i> 921 087 407
            </li>

            <li class="text-white text-xl mb-3">
              <i class="fa-solid fa-envelope mr-3"></i> <a href="mailto:petizosperu@gmail.com">petizosperu@gmail.com</a>
            </li>
          </ul>
        </div>
      </div>

      <div class="col-mb-6 md:mb-0">
        <div class="max-w-md mx-auto">
          <figure class="mx-auto max-w-[256px] mb-1">
            <img src="{{ asset('logo-light.png') }}" alt="Logo" class="min-w-full">
          </figure>

          <p class="text-center text-white font-sans font-bold text-2xl">
            Lima, Perú.
          </p>
        </div>
      </div>
    </div>

    <div class="my-6 w-10/12 mx-auto bg-white/25 h-0.5"></div>

    <p class="text-center text-xl text-white font-sans">
      © {{ now()->format('Y') }} Petizos Perú | Todos los derechos reservados
    </p>
  </div>
</footer>