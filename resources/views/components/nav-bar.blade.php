<nav class="px-4 py-6 w-full {{ !is_null($isHome) && $isHome === 'home' ? 'bg-teal-400' : 'bg-transparent' }}"
x-data="{ isShow: false }">
    <div class="container">

      <div class="w-full flex h-16 flex-row justify-between">
        <a href="{{ route('home') }}">
          <img src="{{ asset('logo.png') }}" alt="logo" class="h-full">
        </a>

        <div class="hidden lg:flex flex-row">
          <ul class="list-none flex flex-row h-full items-center" role="navigation">
            @foreach ($links as $title => $link)
              <li role="menuitem" class="flex">
                <a href="{{ route('home') . $link }}" class="px-6 py-4 font-sans text-xl transition-colors duration-200 hover:text-slate-900 {{ !is_null($isHome) && $isHome === 'home' ? 'text-white' : 'text-slate-700' }}" 
                role="button"
                data-hash-link="true"
                >
                  {{ $title }}
                </a>
              </li>
            @endforeach
          </ul>

          <ul class="ml-4 list-none flex flex-row h-full items-center">
            @foreach ($socialLinks as $icon => $link)
              <li class="flex" role="menuitem">
                <a href="{{ $link }}" class="text-2xl ml-2 hover:text-slate-900 transition-colors duration-200 {{ !is_null($isHome) && $isHome === 'home' ? 'text-white' : 'text-slate-700' }}">
                  <i class="fa-brands {{ $icon }}"></i>
                </a>
              </li>
            @endforeach
          </ul>
        </div>

        <div class="h-full flex lg:hidden items-center">
          <button class="bg-transparent border-0 p-2 hover:text-slate-900 transition-colors duration-200 {{ !is_null($isHome) && $isHome === 'home' ? 'text-white' : 'text-slate-700' }}"
          x-on:click="isShow = true"
          >
            <i class="fa-solid fa-align-right text-3xl"></i>
          </button>
        </div>
      </div>

    </div>

    <div class="bg-black/90 backdrop-blur-md w-screen h-screen fixed top-0 left-0 z-40 flex flex-col transition-transform duration-500" style="display: none;"
    x-show="isShow"
    x-transition:enter-start="translate-x-[105vw]"
    x-transition:enter-end="translate-x-0"
    x-transition:leave-start="translate-x-0"
    x-transition:leave-end="translate-x-[105vw]"
    >

      <div class="row justify-end p-7">
        <div class="col-3 flex justify-center">
          <i class="fa-solid fa-xmark text-4xl text-white inline-block transition-all duration-300 hover:rotate-180 hover:text-rose-300 focus:text-rose-300 focus:rotate-180 focus:outline-0 shadow-lg"
          role="button"
          tabindex="0"
          x-on:click="isShow = false"
          >
          </i>
        </div>
      </div>

      <div class="row items-center p-6 grow">
        <ul class="text-3xl text-white text-center font-bold -mt-4">
          @foreach ($links as $title => $link)
            <li class="mb-4">
              <a href="{{ route('home') }}{{ $link }}"
              class="hover:text-rose-300 focus:text-rose-300 transition-colors duration-300"
              data-hash-link="true"
              x-on:click="isShow = false"
              >
                {{ $title }}
              </a>
            </li>
          @endforeach

          <li class="mt-8">
            <ul class="list-none flex flex-row h-full justify-center">
              @foreach ($socialLinks as $icon => $link)
                <li class="flex" role="menuitem">
                  <a href="{{ $link }}" class="text-4xl ml-3 hover:text-rose-300 transition-colors duration-200"
                  x-on:click="isShow = false"
                  >
                    <i class="fa-brands {{ $icon }}"></i>
                  </a>
                </li>
              @endforeach
            </ul>
          </li>
        </ul>
      </div>

    </div>
</nav>