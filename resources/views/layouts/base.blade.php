<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('titlePage') - Petizos Perú</title>
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
    @vite('resources/css/app.css')
    <!-- Additional CSS -->
    @yield('additionalStyles')
    <!-- Google fonts CSS -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,100;0,300;0,400;0,700;0,900;1,100;1,300;1,400;1,700;1,900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.2/css/all.min.css">
</head>
<body class="overflow-hidden"
x-data
:class="{ 'overflow-hidden': $store.homeStore.ajax_loading }">
    <div class="w-screen h-screen flex justify-center items-center overflow-hidden bg-black/90 backdrop-blur-md fixed top-0 left-0 z-50" id="loading-page">
        <div class="rounded-full w-16 h-16 animate-ping-fast border-8 border-white"></div>
    </div>

    <div class="w-screen h-screen flex justify-center items-center overflow-hidden bg-black/90 backdrop-blur-md fixed top-0 left-0 z-50"
    style="display: none;"
    x-data
    x-show="$store.homeStore.ajax_loading"
    x-transition
    >
        <div>
            <div class="w-36 h-36 relative overflow-hidden rounded-[50%]">
                <div id="container-loader" class="w-full h-full absolute rounded-[50%]"
                :style="{ 'background': $store.homeStore.bg_content_loader, 'transform': $store.homeStore.rotate_content_loader }"
                >
                </div>

                <div id="left-loader" 
                class="absolute h-full w-1/2 z-[60] left-0"
                :style="{ 'background': $store.homeStore.bg_left }"
                >
                </div>

                <div id="right-loader" 
                class="absolute h-full w-1/2 z-[60] right-0"
                :style="{ 'background': $store.homeStore.bg_right }"
                >
                </div>

                <div id="center-loader" class="relative w-[90%] h-[90%] z-[70] bg-black mx-auto top-[5%] rounded-[50%]">
                    <h5 class="absolute top-1/2 left-1/2 text-white -translate-x-1/2 -translate-y-1/2 text-2xl font-sans font-bold pl-2"
                    x-text="$store.homeStore.current_percentage + '%'"
                    >
                    </h5>
                </div>
            </div>
        </div>
    </div>

    <x-nav-bar :links="$navbar_links" :social-links="$navbar_social" />

    <div class="fixed z-50 top-6 right-6"
    x-show="$store.homeStore.alerts.length > 0"
    >
        <template x-for="(alert, index) in $store.homeStore.alerts"
        :key="'alert-' + alert.type + index">
            <div class="p-7 rounded mb-4 last:mb-0 shadow-md max-w-xs transition-all duration-300 ease-out hover:shadow-xl relative"
            :class="{ 'bg-emerald-300': alert.type === 'success', 'bg-red-300': alert.type === 'danger' }"
            >
                <p class="text-lg font-sans select-none"
                :class="{ 'text-emerald-900': alert.type === 'success', 'text-red-900': alert.type === 'danger' }"
                x-text="alert.message"
                >
                </p>

                <span class="inline-block px-2 text-sm cursor-pointer select-none font-bold absolute top-2 right-2"
                :class="{ 'text-emerald-900': alert.type === 'success', 'text-red-900': alert.type === 'danger' }"
                @click="$store.homeStore.alerts.splice(index, 1)"
                >
                    <i class="fa-solid fa-xmark"></i>
                </span>
            </div>
        </template>
    </div>

    @yield('content')

    <x-footer :social="$footer_social" />

    @vite('resources/js/app.js')
</body>
</html>