<body style="box-sizing: border-box; padding: 32px;">
    <h1 style="font-family: 'sans-serif'; font-size: 2rem; color: #dc2626; margin-bottom: 1.75rem; text-align: center;">Nuevo Pedido Registrado.</h1>
    <p style="line-height: 1.125rem; font-size: .85rem; font-family: 'sans-serif'; color: #1f2937; padding: 1rem; margin-bottom: 1.75rem;">
        Tienes un nuevo pedido con los siguientes detalles:
    </p>
    <ul style="text-align: center; list-style: none; line-height: 1.125rem; font-size: .85rem;">
        <li style="margin-bottom: 1.25rem;">
            <b>Tipo De Mascota: </b> {{ $pet }}.
        </li>
        <li style="margin-bottom: 1.25rem;">
            <b>Tamaño: </b> {{ $size }} cm.
        </li>
        <li style="margin-bottom: 1.25rem;">
            <b>Diseño: </b> {{ $design }}.
        </li>
        @if(isset($name))
            <li style="margin-bottom: 1.25rem;">
              <b>Nombre De La Mascota: </b> {{ $name }}.
            </li>
        @endif
        <li style="margin-bottom: 1.25rem;">
            <b>Datos De Contacto: </b> {{ $contact }}.
        </li>
    </ul>

    @if(!is_null($note) && $note !== 'null')
        <h5 style="font-family: 'Lato',  'Segoe UI', Roboto, 'Helvetica Neue', Arial, 'Noto Sans', sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol', 'Noto Color Emoji'; font-size: 1.125rem; color: #dc2626; margin-bottom: 1.25rem; text-align: center;">Notas Al Diseñador:</h5>
        <p style="text-align: center;">
            {{ $note }}
        </p>
    @endif
</body>